<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title>Title</title>


    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">





</head>

<body>
    <!--PRELOADER START-->

    <!--    [ Strat Preloader Area]-->
    <!--<div class="pre-loader-area">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>-->


    <!--    [Finish Poreloader Area]-->
    <!--    [ Strat Header Area]-->
    <header>
        <!--    [ Strat Logo Area]-->
        <div class="main-menu-area">
            <div class="top-menu d-table">
                <div class="top-menu-content d-table-cell">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="top-contant">
                                    <p><i class="icofont icofont-email"></i> info@youthub.com</p>
                                    <p><i class="icofont icofont-telephone"></i> 996 - 5553 - 453</p>
                                </div>
                            </div>
                            <div class="col-lg-6 text-right">
                                <div class="social">
                                    <a href="#"><i class="icofont icofont-social-facebook"></i></a>
                                    <a href="#"><i class="icofont icofont-social-twitter"></i></a>
                                    <a href="#"><i class="icofont icofont-social-google-plus"></i></a>
                                    <a href="#"><i class="icofont icofont-social-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="main-menu">
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="index.php">
                            <span class="logo">
                                <img src="assets/img/logo.png" alt="">
                            </span>
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">About</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Learn Online</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Skills</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">RMG</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">NRB</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Digital Youth Fest</a>
                                </li>
                                <li class="nav-item pulse-button ">
                                    <a class="nav-link" href="blog.php">Blog</a>
                                </li>
                            </ul>
                            <ul class="nav justify-content-end maon-menu-cart">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#" title="SEARCH"><i class="icofont icofont-search"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#" title="LOG IN"><i class="icofont icofont-business-man-alt-1"></i></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>

        </div>
        <!--    [Finish Logo Area]-->
    </header>
    <!--    [Finish Header Area]-->
