<?php require('header.php'); ?>

<!--    [ Strat Section Title Area]-->
<section id="welcome">
    <div id="youtubevideo" class="player" data-property="{videoURL:'UBufeh1yv2c',containment:'#video',autoPlay:true, mute:false, startAt:0, showControls:false, loop:true, opacity:1}"></div>
    <div class="welcome-carousels owl-carousel">
        <div class="carousel-content carousel-bg-1 d-table">
            <div class="single-carousel d-table-cell">
                <div class="container">
                    <h2>A land of Youth, Versatility, <span>Resilience &amp; Endless Opportunity.</span></h2>
                    <p><strong>Half Jacket + Skiny Trousers + Boot leather</strong></p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry </p>
                </div>
            </div>
        </div>
        <div class="carousel-content carousel-bg-2 d-table">
            <div class="single-carousel d-table-cell">
                <div class="container">
                    <h2>A land of Youth, Versatility, <span>Resilience &amp; Endless Opportunity.</span></h2>
                    <p><strong>Half Jacket + Skiny Trousers + Boot leather</strong></p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry </p>
                </div>
            </div>
        </div>
        <div class="carousel-content carousel-bg-3 d-table">
            <div class="single-carousel d-table-cell">
                <div class="container">
                    <h2>A land of Youth, Versatility, <span>Resilience &amp; Endless Opportunity.</span></h2>
                    <p><strong>Half Jacket + Skiny Trousers + Boot leather</strong></p>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->

<!--    [ Strat Section Area]-->
<section id="feature">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 text-center">
                    <div class="single-feature">
                        <div class="feature-content">
                            <div class="pulse">
                                <i class="icofont icofont-learn"></i>
                                <!--<i class="icofont icofont-presentation-alt"></i>-->
                            </div>
                            <h4>Online Learning</h4>
                            <p>Learning Management System for WordPress.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 text-center">
                    <div class="single-feature">
                        <div class="feature-content">
                            <div class="pulse">
                                <i class="icofont icofont-presentation-alt"></i>
                            </div>

                            <h4>Live Session</h4>
                            <p>Learning Management System for WordPress.</p>
                        </div>

                    </div>
                </div>
                <div class="col-lg-4 text-center">
                    <div class="single-feature">
                        <div class="feature-content">
                            <div class="pulse">
                                <i class="icofont icofont-dashboard-web"></i>
                            </div>
                            <h4>Skills Develope</h4>
                            <p>Learning Management System for WordPress.</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->



<!--    [ Strat Session Section Area]-->
<section id="session">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <div class="section-title">
                        <h3><span>Cource </span>Schedule</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                    </div>
                </div>
            </div>
            <div class="session-content">
                <div class="row">
                    <div class="col-lg-3 paddingRnone">
                        <div class="session-menu">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">
                                    <div class="sec-txt-hight">Business Communation</div>
                                </a>
                                <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                                    <div class="sec-txt-hight">Strategic Management</div>
                                </a>
                                <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                                    <div class="sec-txt-hight">Digital Marketing</div>
                                </a>
                                <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">
                                    <div class="sec-txt-hight">Online Marketing</div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 padding-none">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <div class="sessioner-details-content">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule01.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content bg-dark">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule02.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule03.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content bg-dark">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule04.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                <div class="sessioner-details-content">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule02.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content bg-dark">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule01.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule03.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content bg-dark">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule04.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                <div class="sessioner-details-content">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule04.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content bg-dark">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule03.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule02.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content bg-dark">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule01.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                <div class="sessioner-details-content">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule01.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content bg-dark">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule02.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule03.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="sessioner-details-content bg-dark">
                                    <div class="row padding-none">
                                        <div class="col-lg-2 text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-user">
                                                        <img src="assets/img/sedule04.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 padding-none text-center">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn session-partition d-table-cell">
                                                    <div class="session-time">
                                                        <p>9:30 AM - 11:30 AM </p>
                                                        <p>Course Length: 60 Hour</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-7">
                                            <div class="single-session d-table">
                                                <div class="single-session-comn d-table-cell">
                                                    <div class="session-single-content">
                                                        <h6>Vestibulum Vitae Malesuada Nunc.</h6>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sed purus a libero ultricies semper. Nunc varius diam et ornare mollis. Vivamus</p>
                                                        <p class="session-host">Alica Foxy - Web Dsigner.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Session Section Area]-->

<!--    [ Strat Speaker Section Area]-->
<section id="speaker">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <div class="section-title">
                        <h3><span>Our </span>Instructors</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                    </div>
                </div>
            </div>
            <div class="speakers-list">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="single-speaker-info">
                            <div class="single-speaker-pic">
                                <img src="assets/img/speaker01.jpg" alt="">
                            </div>
                            <div class="single-speaker-txt">
                                <h4>Nathan Johns <span>Business - Main Speaker</span></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-speaker-info">
                            <div class="single-speaker-pic middle-speaker">
                                <img src="assets/img/speaker02.jpg" alt="">
                            </div>
                            <div class="single-speaker-txt">
                                <h4>Nathan Johns <span>Business - Main Speaker</span></h4>
                            </div>
                        </div>

                        <div class="single-speaker-info">
                            <div class="single-speaker-pic middle-speaker">
                                <img src="assets/img/speaker03.jpg" alt="">
                            </div>
                            <div class="single-speaker-txt">
                                <h4>Nathan Johns <span>Business - Main Speaker</span></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-speaker-info">
                            <div class="single-speaker-pic">
                                <img src="assets/img/speaker04.jpg" alt="">
                            </div>
                            <div class="single-speaker-txt">
                                <h4>Nathan Johns <span>Business - Main Speaker</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Speaker Section Area]-->


<!--    [ Strat Section Area]-->
<section id="blog">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <div class="section-title">
                        <h3><span>Our</span> Blog</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                    </div>
                </div>
            </div>
            <div class="blog-content">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="single-blog">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-responsive" src="assets/img/blog01.jpg" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="bl-author-part">
                                        <p><i class="icofont icofont-user-alt-3"></i> Author Name</p>
                                    </div>
                                    <div class="bl-author-part text-right">
                                        <p><i class="icofont icofont-calendar"></i> 22 December, 2017</p>
                                    </div>
                                    <div class="single-blog-content">
                                        <h4>Contrary to popular belief, Contrary to popular belief</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit quae facilis harum, sed et est beatae dignissimos. Veritatis, impedit. Voluptatibus.</p>
                                        <a href="single-blog.php" class="read-more">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-blog">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-responsive" src="assets/img/blog02.jpg" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="bl-author-part">
                                        <p><i class="icofont icofont-user-alt-3"></i> Author Name</p>
                                    </div>
                                    <div class="bl-author-part text-right">
                                        <p><i class="icofont icofont-calendar"></i> 22 December, 2017</p>
                                    </div>
                                    <div class="single-blog-content">
                                        <h4>Contrary to popular belief, Contrary to popular belief</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit quae facilis harum, sed et est beatae dignissimos. Veritatis, impedit. Voluptatibus.</p>
                                        <a href="single-blog.php" class="read-more">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="single-blog">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-responsive" src="assets/img/blog03.jpg" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="bl-author-part">
                                        <p><i class="icofont icofont-user-alt-3"></i> Author Name</p>
                                    </div>
                                    <div class="bl-author-part text-right">
                                        <p><i class="icofont icofont-calendar"></i> 22 December, 2017</p>
                                    </div>
                                    <div class="single-blog-content">
                                        <h4>Contrary to popular belief.</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit quae facilis harum, sed et est beatae dignissimos. Veritatis, impedit. Voluptatibus.</p>
                                        <a href="single-blog.php" class="read-more">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat News Latter Section Area]-->
<section id="newslatter">
    <div class="container">
        <div class="news-latter-content">
            <div class="row justify-content-between">
                <div class="col-lg-6">
                    <div class="news-single-title">
                        <h5>NEWS LETTER <span>join us now to get all news and special offers</span></h5>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="subscrib-forn">
                        <form action="">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="icofont icofont-ui-email"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="type your email here" aria-label="Username" aria-describedby="basic-addon1">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button">Join us</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish News Latter Section Area]-->


<!--    [ Strat Best Sale Section Area]-->
<section id="about">
    <div id="youtubevideo" class="player" data-property="{videoURL:'UBufeh1yv2c',containment:'#video',autoPlay:true, mute:false, startAt:0, showControls:false, loop:true, opacity:1}"></div>
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <div class="section-title">
                        <h3><span>About</span> YoutHub</h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                    </div>
                </div>
            </div>
            <div class="about-content">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="about-logo">
                            <img class="img-responsive" src="assets/img/logo2.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="single-about-content">
                            <h5>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga ullam voluptas culpa repudiandae amet, iste asperiores natus sapiente accusantium. In laboriosam delectus esse voluptate, dolore maxime expedita saepe voluptatibus ad vel corporis pariatur debitis, minus adipisci? Cupiditate voluptatibus facere quo asperiores, consequuntur necessitatibus quas soluta! Ad animi delectus harum beatae odit at eaque deserunt, fugiat voluptate, voluptatum praesentium dolores molestiae.</p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga ullam voluptas culpa repudiandae amet, iste asperiores natus sapiente accusantium. In laboriosam delectus esse voluptate, dolore maxime expedita saepe voluptatibus ad vel corporis pariatur debitis, minus adipisci? Cupiditate voluptatibus facere quo asperiores, consequuntur necessitatibus quas soluta! Ad animi delectus harum beatae odit at eaque deserunt, fugiat voluptate, voluptatum praesentium dolores molestiae.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Best Sale Section Area]-->

<?php require('footer.php'); ?>
