<?php require('header.php'); ?>

<!--    [ Strat Section Area]-->
<section id="blog">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title">
                        <h3><span>Our</span> Blog</h3>
                        <p>Home > Blog</p>
                    </div>
                </div>
            </div>
            <div class="blog-content">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="single-blog">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-responsive" src="assets/img/blog01.jpg" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="bl-author-part">
                                        <p><i class="icofont icofont-user-alt-3"></i> Author Name</p>
                                    </div>
                                    <div class="bl-author-part text-right">
                                        <p><i class="icofont icofont-calendar"></i> 22 December, 2017</p>
                                    </div>
                                    <div class="single-blog-content">
                                        <h4>Contrary to popular belief, Contrary to popular belief</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit quae facilis harum, sed et est beatae dignissimos. Veritatis, impedit. Voluptatibus.</p>
                                        <a href="single-blog.php" class="read-more">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="single-blog">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-responsive" src="assets/img/blog02.jpg" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="bl-author-part">
                                        <p><i class="icofont icofont-user-alt-3"></i> Author Name</p>
                                    </div>
                                    <div class="bl-author-part text-right">
                                        <p><i class="icofont icofont-calendar"></i> 22 December, 2017</p>
                                    </div>
                                    <div class="single-blog-content">
                                        <h4>Contrary to popular belief, Contrary to popular belief</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit quae facilis harum, sed et est beatae dignissimos. Veritatis, impedit. Voluptatibus.</p>
                                        <a href="single-blog.php" class="read-more">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="single-blog">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-responsive" src="assets/img/blog03.jpg" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="bl-author-part">
                                        <p><i class="icofont icofont-user-alt-3"></i> Author Name</p>
                                    </div>
                                    <div class="bl-author-part text-right">
                                        <p><i class="icofont icofont-calendar"></i> 22 December, 2017</p>
                                    </div>
                                    <div class="single-blog-content">
                                        <h4>Contrary to popular belief.</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit quae facilis harum, sed et est beatae dignissimos. Veritatis, impedit. Voluptatibus.</p>
                                        <a href="single-blog.php" class="read-more">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="single-blog">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-responsive" src="assets/img/blog01.jpg" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="bl-author-part">
                                        <p><i class="icofont icofont-user-alt-3"></i> Author Name</p>
                                    </div>
                                    <div class="bl-author-part text-right">
                                        <p><i class="icofont icofont-calendar"></i> 22 December, 2017</p>
                                    </div>
                                    <div class="single-blog-content">
                                        <h4>Contrary to popular belief, Contrary to popular belief</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit quae facilis harum, sed et est beatae dignissimos. Veritatis, impedit. Voluptatibus.</p>
                                        <a href="single-blog.php" class="read-more">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="single-blog">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-responsive" src="assets/img/blog02.jpg" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="bl-author-part">
                                        <p><i class="icofont icofont-user-alt-3"></i> Author Name</p>
                                    </div>
                                    <div class="bl-author-part text-right">
                                        <p><i class="icofont icofont-calendar"></i> 22 December, 2017</p>
                                    </div>
                                    <div class="single-blog-content">
                                        <h4>Contrary to popular belief, Contrary to popular belief</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit quae facilis harum, sed et est beatae dignissimos. Veritatis, impedit. Voluptatibus.</p>
                                        <a href="single-blog.php" class="read-more">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="single-blog">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-responsive" src="assets/img/blog03.jpg" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="bl-author-part">
                                        <p><i class="icofont icofont-user-alt-3"></i> Author Name</p>
                                    </div>
                                    <div class="bl-author-part text-right">
                                        <p><i class="icofont icofont-calendar"></i> 22 December, 2017</p>
                                    </div>
                                    <div class="single-blog-content">
                                        <h4>Contrary to popular belief.</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit quae facilis harum, sed et est beatae dignissimos. Veritatis, impedit. Voluptatibus.</p>
                                        <a href="s" class="read-more">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php require('footer.php'); ?>
