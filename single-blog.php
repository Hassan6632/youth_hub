<?php require('header.php'); ?>

<!--    [ Strat Section Area]-->
<section id="single-blog">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section-title">
                        <h3><span>Our</span> Blog</h3>
                        <p>Home > Blog</p>
                    </div>
                </div>
            </div>
            <div class="blog-content">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="single-blog">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-responsive" src="assets/img/blog01.jpg" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="bl-author-part">
                                        <p><i class="icofont icofont-user-alt-3"></i> Author Name</p>
                                    </div>
                                    <div class="bl-author-part text-right">
                                        <p><i class="icofont icofont-calendar"></i> 22 December, 2017</p>
                                    </div>
                                    <div class="single-blog-content">
                                        <h4>Contrary to popular belief, Contrary to popular belief</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit quae facilis harum, sed et est beatae dignissimos. Veritatis, impedit. Voluptatibus.</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam a, similique voluptates amet impedit reiciendis eligendi ea aperiam quos asperiores odio suscipit dolore deserunt voluptatum ad provident quas, quibusdam. Quia natus tenetur, exercitationem. Modi in consequuntur, sint quam eos, tempora debitis nihil voluptas deserunt beatae dignissimos recusandae, omnis itaque explicabo suscipit totam temporibus numquam repellat ipsam vero aperiam unde. Dolore, repellat vero hic a autem delectus quae error, natus perspiciatis asperiores, ad aut! At, suscipit consequatur porro dolore iure mollitia provident ab voluptas nihil sequi cum neque eaque. Suscipit earum obcaecati impedit optio pariatur repellat dignissimos minus voluptates eum nostrum.</p>
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam a, similique voluptates amet impedit reiciendis eligendi ea aperiam quos asperiores odio suscipit dolore deserunt voluptatum ad provident quas, quibusdam. Quia natus tenetur, exercitationem. Modi in consequuntur, sint quam eos, tempora debitis nihil voluptas deserunt beatae dignissimos recusandae, omnis itaque explicabo suscipit totam temporibus numquam repellat ipsam vero aperiam unde. Dolore, repellat vero hic a autem delectus quae error, natus perspiciatis asperiores, ad aut! At, suscipit consequatur porro dolore iure mollitia provident ab voluptas nihil sequi cum neque eaque. Suscipit earum obcaecati impedit optio pariatur repellat dignissimos minus voluptates eum nostrum.
                                        </p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="widget">
                            <div class="side-bar-title text-right">
                                <h5>Search</h5>
                            </div>
                            <div class="widget-search">
                                <div class="subscrib-forn">
                                    <form action="">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search" aria-label="Username" aria-describedby="basic-addon1">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button"><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="side-bar-title text-right">
                                <h5>Tags</h5>
                            </div>
                            <div class="tags text-right">
                                <ul>
                                    <li><a href="#">Banglades</a></li>
                                    <li><a href="#">Plab</a></li>
                                    <li><a href="#">YoutHub</a></li>
                                    <li><a href="#">Live</a></li>
                                    <li><a href="#">Learn</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="side-bar-title text-right">
                                <h5>Short Link</h5>
                            </div>
                            <div class="tags text-right">
                                <ul>
                                    <li><a href="#">Banglades</a></li>
                                    <li><a href="#">Plab</a></li>
                                    <li><a href="#">YoutHub</a></li>
                                    <li><a href="#">Live</a></li>
                                    <li><a href="#">Learn</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->

<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->

<?php require('footer.php'); ?>
