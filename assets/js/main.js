(function ($) {
    "use strict";

    jQuery(document).ready(function () {


        /*  HOME Carousel
        ----------------------------------*/
        $(".welcome-carousels").owlCarousel({
            items: 1,
            dots: true,
            autoplay: true,
            nav: false,
            loop: true,

        });
        if ($.fn.on) {
            $(".welcome-carousels").on("translate.owl.carousel", function () {
                $(".single-carousel h2").removeClass("animated fadeInUp").css("opacity", "0")
            });
            $(".welcome-carousels").on("translated.owl.carousel", function () {
                $(".single-carousel h2").addClass("animated fadeInUp").css("opacity", "1")
            });
        }







        /*  - Scroll-top 
      	---------------------------------------------*/
        jQuery(window).on('scroll', function () {
            var scrollTop = jQuery(this).scrollTop();
            if (scrollTop > 400) {
                jQuery('.top').fadeIn();
            } else {
                jQuery('.top').fadeOut();
            }
        });

        jQuery('.top').on('click', function () {
            $('html, body').animate({
                scrollTop: 0
            }, 1000);

            return false;
        });






    });


    jQuery(window).on("load", function () {
        /*  - Pre Loader
        ---------------------------------------------*/
        $(".pre-loader-area").fadeOut();

    });


}(jQuery));
