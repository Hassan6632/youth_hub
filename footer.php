<!--    [ Start Footer Area]-->
<footer>
    <div class="copy-right">
        <div class="container">
            <div class="footer-top">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="footer-logo">
                            <img src="assets/img/logo.png" alt="">
                            <p>shopy c 2015 . your copy right here</p>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-2">
                        <ul class="footer-menu">
                            <li><a href="#">about us</a></li>
                            <li><a href="#">contact us</a></li>
                            <li><a href="#">support</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <ul class="footer-menu">
                            <li><a href="#">our feed</a></li>
                            <li><a href="#">terms and conditions</a></li>
                            <li><a href="#">our privacy</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <ul class="footer-menu">
                            <li><a href="#">join us</a></li>
                            <li><a href="#">live support</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2">
                        <div class="footer-pay">
                            <h5>Payment Methods</h5>
                            <img src="assets/img/pay.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="copy-context">
                        <p>&copy; 2018. All Rights Reserved. Developed By <span><a href="http://preneurlab.com/"><img src="assets/img/prelab.png" alt=""></a></span></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>
<!--    [Finish Footer Area]-->

<!--SCROLL TOP BUTTON-->
<a href="#" class="top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>




<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<!--    [jQuery]-->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<!--    [Popper Js] -->
<script src="assets/js/popper.min.js"></script>

<!--    [Bootstrap Js] -->
<script src="assets/js/bootstrap.min.js"></script>

<!--    [yOUTUBE bg vIDEO Js]-->
<script src="assets/js/jquery.mb.YTPlayer.min.js"></script>

<!--    [OwlCarousel Js]-->
<script src="assets/js/owl.carousel.min.js"></script>


<!--    [Main Custom Js] -->
<script src="assets/js/main.js"></script>
<script>
    /*  YouTube Video Player
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    ----------------------------------*/
    if ($.fn.YTPlayer) {
        $(".player").YTPlayer();
    }

</script>
</body>

</html>
